package com.example.binartestteja.modal.model

data class ModelPesan(
    val show: Boolean=false,
    val tittle: String,
    val message: String,
    val isFinish: Boolean
)
