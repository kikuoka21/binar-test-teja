package com.example.binartestteja.modal

import android.app.Application
import com.example.binartestteja.AppComponent
import com.example.binartestteja.DaggerAppComponent

import timber.log.Timber

class MyApplication : Application(){
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        appComponent.inject(this)

        Timber.plant(Timber.DebugTree())

        Timber.d("================================INITIAL timber===================================")
    }
}