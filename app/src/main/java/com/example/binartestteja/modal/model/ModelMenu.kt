package com.example.binartestteja.modal.model

data class ModelMenu(
    val idMenuCategory: String,
    val namaMenuCategory: String,
    val idDrawableMenu: Int
)
