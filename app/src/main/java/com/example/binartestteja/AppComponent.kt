package com.example.binartestteja

import com.example.binartestteja.modal.AppModule
import com.example.binartestteja.modal.MyApplication
import com.example.binartestteja.screen.activity2.MainSources
import com.example.binartestteja.screen.activity2.SourcesViewModel
import com.example.binartestteja.screen.activity3.MainNews
import com.example.binartestteja.screen.activity3.NewsViewModel
import com.example.binartestteja.screen.activity4.MainWebView
import dagger.Component

@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(application: MyApplication)

    fun inject(activity: MainSources)
    fun inject(activity: SourcesViewModel)

    fun inject(activity: MainNews)
    fun inject(activity: NewsViewModel)

    fun inject(activity: MainWebView)
}